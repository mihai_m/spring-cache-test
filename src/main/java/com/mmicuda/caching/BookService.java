package com.mmicuda.caching;

import java.util.List;

public interface BookService {

    Book loadBook(String title);

    List<Book> loadByAuthor(String author);

}
