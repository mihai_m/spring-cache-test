package com.mmicuda.caching;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class DummyBookService implements BookService {

    private static List<Book> books = new ArrayList<>();

    static {
        books.add(new Book("book", "author"));
    }

    @Override
    @Cacheable("book")
    public Book loadBook(String title) {
        simulateDelay();
        log.info("got book by title");
        return books.get(0);
    }

    @Override
    @Cacheable("books")
    public List<Book> loadByAuthor(String author) {
        simulateDelay();
        log.info("got book by author");
        return books.stream().filter(b -> b.getAuthor().equals(author)).collect(Collectors.toList());
    }

    private void simulateDelay() {
        try {
            System.out.println("delaying");
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
