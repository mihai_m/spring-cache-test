package com.mmicuda.caching;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AppRunner implements CommandLineRunner {

    private BookService bookService;

    @Autowired
    public AppRunner(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("starting to do stuff");
        while(true) {
            log.info("getting books");
            bookService.loadBook("asad");
            bookService.loadByAuthor("asdad");

            Thread.sleep(2000);
        }
    }
}
